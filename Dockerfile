# FROM maven:3-jdk-8-alpine as builder

# WORKDIR /usr/src/app

# COPY . /usr/src/app
# RUN mvn package

# FROM openjdk:8-jre-alpine

# COPY --from=builder /usr/src/app/target/*.jar /app.jar

# EXPOSE 8080

# ENTRYPOINT ["java"]
# CMD ["-jar", "/app.jar"]

# Builder Stage
FROM maven:3-jdk-8-alpine as builder

WORKDIR /usr/src/app

# Copy the pom.xml and download dependencies first for better caching
COPY pom.xml ./
RUN mvn dependency:go-offline

# Copy the rest of the application source code
COPY . ./

# Build the application
RUN mvn package

# Final Stage
FROM openjdk:8-jre-alpine

# Copy the JAR file from the builder stage
COPY --from=builder /usr/src/app/target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]